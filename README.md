# vscode-michelson README

This a test release of an Michelson extension.
This extension may be unstable. As such, please
do not hesitate to open an issue explaining the troubles
you're encountering.

## Features

- typecheck
- documentation
- completion
- type stack printing

## Requirements

Right now, it only works on linux.

## Extension Settings

<!-- This extension contributes the following settings: -->

## Install

Get the .vsix in the releases page, then run
`code --install-extension vscode-michelson-0.0.1.vsix`