import * as vscode from 'vscode';

export default class StackProvider implements vscode.TextDocumentContentProvider {

  static scheme = "stack";

  private _onDidChange = new vscode.EventEmitter<vscode.Uri>();

  constructor () {}

  get onDidChange() {
    return this._onDidChange.event;
  }


  provideTextDocumentContent(uri: vscode.Uri): Thenable<string> {
    const [target, pos] = decodeLocation(uri);
    return vscode.commands.executeCommand('extension.get_stack', target, pos).then(stack => {
      if (typeof(stack) !== "string") { return ""; }
      return stack;
    });
  }

}

let seq = 0;

export function encodeLocation(uri:vscode.Uri, pos: vscode.Position): vscode.Uri {
  const query = JSON.stringify([uri.toString(), pos.line, pos.character]);
  return vscode.Uri.parse(`${StackProvider.scheme}:location?${query}#${seq++}`);
}

export function decodeLocation(uri: vscode.Uri): [vscode.Uri, vscode.Position] {
  let [target, line, character] = <[string, number, number]>JSON.parse(uri.query);
  return [vscode.Uri.parse(target), new vscode.Position(line, character)];
}