// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import * as path from 'path';
import { LanguageClient, TransportKind } from "vscode-languageclient";
import { existsSync } from 'fs';
import stackProvider, { encodeLocation } from './stack_provider';

interface Stack {
	after: string;
}

// Find where the langserver binary is
function get_location(context: vscode.ExtensionContext): string | null {
	var binary_location: any = vscode.workspace.getConfiguration('michelson_language_server').get("location");
	if (!binary_location) {
		let path = context.extensionPath + '/lang-server/bin-linux';
		if (!existsSync(path)) {
			vscode.window.showErrorMessage("Michelson Language Server not found");
			return null;
		} else {
			return path;
		}
		return null;
	} else {
		if (!binary_location.startsWith('/')) {
			binary_location = path.join(vscode.workspace.getWorkspaceFolder.toString(), binary_location);
		}
		if (!existsSync(binary_location)) {
			vscode.window.showErrorMessage("Michelson language server not found @" + binary_location);
			return null;
		}
	}
	return binary_location;
}


var client: LanguageClient | null = null;

function configure_language() {
	return vscode.languages.setLanguageConfiguration('michelson', {

	});
}

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "vscode-michelson" is now active! But maybe not.');


	function start() {
		let client_options = {
			documentSelector: [
				{ scheme: 'file', language: 'michelson' }
			],
		};
		let location = get_location(context);
		if (!location) { return; }
		client = new LanguageClient(
			"michelson-language-server",
			"Michelson Language Server",
			{
				command: location,
				args: [],
				transport: TransportKind.stdio
			},
			client_options,
			true
		);
		context.subscriptions.push(configure_language());
		context.subscriptions.push(client.start());
	}

	start();

	function get_stack(target: vscode.Uri, pos: vscode.Location) {
		let payload = {
			cursor: {start: pos, end: pos},
			uri: target.toString(),
		};
		if (!client) {
			return Promise.reject("Michelson language client is not running");
		}
		return client.sendRequest('custom:showStack', payload).then(async (res) => {
			// if (typeof(res) !== "object") { return; }
			let after = (res as Stack).after;
			return after;
		});
	}

	context.subscriptions.push(vscode.commands.registerCommand('extension.get_stack', get_stack));

	function show_stack(e: vscode.TextEditorSelectionChangeEvent) {
		if (e.textEditor.document.languageId !== "michelson") {
			return null;
		}
		let loc = e.selections[0].anchor;
		let doc_uri = e.textEditor.document.uri;
		// FIXME: Shouldn't the 
		let uri = encodeLocation(doc_uri, loc);
		vscode.commands.executeCommand('extension.showStack', uri);
	}

	vscode.workspace.registerTextDocumentContentProvider("stack", new stackProvider);
	vscode.window.onDidChangeActiveTextEditor(e => {
		if (e === undefined) {
			return;
		}
		if (e.document.languageId !== "michelson") {
			let viewColumn = e.viewColumn;
			if (viewColumn === undefined) { return; }
			let editor = vscode.window.visibleTextEditors[viewColumn];
			if (editor === undefined) { return; }
			return null;
		}
	});

	vscode.commands.registerCommand('extension.showStack', () => {
		let editor = vscode.window.activeTextEditor;
		if (editor === undefined) { return; }
		let uri = encodeLocation(editor.document.uri, editor.selection.active);
		// This is open to the side
		return vscode.workspace.openTextDocument(uri).then(doc => 
			vscode.window.showTextDocument(doc, vscode.window.activeTextEditor?.viewColumn! + 1, true));
	});

	vscode.window.onDidChangeTextEditorSelection(show_stack);

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with registerCommand
	// The commandId parameter must match the command field in package.json
	let disposable = vscode.commands.registerCommand('extension.helloWorld', () => {
		// The code you place here will be executed every time your command is executed

		// Display a message box to the user
		vscode.window.showInformationMessage('Hello World!');
	});

	context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
export function deactivate() {
	console.log("I'm deactivated");
	if (client) {
		client.stop();
	}
}
